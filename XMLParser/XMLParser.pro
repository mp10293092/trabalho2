# Application info
TEMPLATE = app
DESTDIR = bin
TARGET = XMLParser
VERSION = 1.0
CONFIG -= c++11

# Qt config
CONFIG += console
CONFIG -= qt app_bundle

# Optimization flags
QMAKE_CXXFLAGS  -= -O -O1
QMAKE_CXXFLAGS  += -O2 -ansi

# Libs
LIBS +=

# Temporary dirs
OBJECTS_DIR = tmp/obj
MOC_DIR = tmp/moc
UI_DIR = tmp/moc
RCC_DIR = tmp/rc

# Source files
INCLUDEPATH *= . src

SOURCES += \
    src/main.c

# Header files
HEADERS +=

# Other files
DISTFILES += \
    Makefile
