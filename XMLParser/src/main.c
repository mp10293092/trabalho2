#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define STR_UNDEF "undef"
#define STR_SIM "Sim"
#define STR_NAO "Não"
#define STR_DELIMITER ':'

#define KEY_CODIGO "Codigo"
#define KEY_FINALIDADE "Finalidade"
#define KEY_TIPO "Tipo"
#define KEY_TERRENO "Terreno"
#define KEY_DIMENSAO "Dimensao"
#define KEY_LARGURA "Largura"
#define KEY_COMPRIMENTO "Comprimento"
#define KEY_AREA "Area"
#define KEY_CONSTRUCAO "Construcao"
#define KEY_ENDERECO "Endereco"
#define KEY_RUA "Rua"
#define KEY_NUMERO "Numero"
#define KEY_BAIRRO "Bairro"
#define KEY_CIDADE "Cidade"
#define KEY_CONTATO "Contato"
#define KEY_NOME "Nome"
#define KEY_TELEFONE "Telefone"
#define KEY_DESCRICAO "Descricao"
#define KEY_QUARTOS "Quartos"
#define KEY_BANHEIROS "Banheiros"
#define KEY_GARAGENS "Garagens"
#define KEY_SUITES "Suites"
#define KEY_SALAS "Salas"
#define KEY_ESCRITORIO "Escritorio"
#define KEY_PISCINA "Piscina"
#define KEY_BANHEIRA "Banheira"
#define KEY_ARMARIO "Armario"
#define KEY_CHURRASQUEIRA "Churrasqueira"
#define KEY_QUINTAL "Quintal"
#define KEY_VALOR "Valor"
#define KEY_FINANCIAMENTO "Financiamento"
#define KEY_IPTU "IPTU"
#define KEY_CONDOMINIO "Condominio"

/* Utility: split string function */
int strsplit2(char *str, char delimiter, char *str1, char *str2) {
    unsigned len = strlen(str);

    /* Find delimiter */
    unsigned i=0;
    for(i=0; i<len; i++) {

        /* Check char for delimiter */
        if(str[i]==delimiter) {

            /* Copy strings splited */
            memcpy(str1, &(str[0]), i+1);
            memcpy(str2, &(str[i+1]), len-i);

            /* Insert \0 */
            str1[i] = '\0';
            str2[len-i] = '\0';

            return EXIT_SUCCESS;
        }
    }

    return EXIT_FAILURE;
}

/* Utility: string trim (remove start/end white spaces) */
void strtrim(char *str) {

    /* Check start white space */
    while(str[0]==' ') {
        unsigned len = strlen(str);

        /* Shift entire string */
        unsigned i=0;
        for(i=0; i<len; i++)
            str[i] = str[i+1];
        len--;
    }

    /* Check end white space */
    unsigned len = strlen(str);
    while(str[len-1]==' ') {
        /* Insert \0 */
        str[len-1] = '\0';
        len--;
    }
}

/* Utility: string equals */
int strequals(char *str1, char *str2) {
    char tmp1[strlen(str1)+1];
    char tmp2[strlen(str2)+1];

    /* Lower case str1 */
    char *p;
    strcpy(tmp1, str1);
    for(p=tmp1; *p; p++) *p = tolower(*p);

    /* Lower case str2 */
    strcpy(tmp2, str2);
    for(p=tmp2; *p; p++) *p = tolower(*p);

    return (strcmp(tmp1, tmp2)==0);
}

/* Model definition */
typedef struct {
    char codigo[8];
    char finalidade[8];
    char tipo[12];
    char dimensao[32];
    char area[5];
    char construcao[5];
    char endereco[128];
    char contato[128];
    char descricao[256];
    char quartos[3];
    char banheiros[3];
    char garagens[3];
    char suites[3];
    char salas[3];
    char escritorio[5];
    char piscina[5];
    char banheira[5];
    char armario[5];
    char churrasqueira[5];
    char quintal[5];
    char valor[32];
    char financiamento[5];
    char iptu[32];
    char condominio[32];
} Model;

/* Model initialization with default/undef values */
void model_initialize(Model *model) {
    strcpy(model->codigo, STR_UNDEF);
    strcpy(model->finalidade, STR_UNDEF);
    strcpy(model->tipo, STR_UNDEF);
    strcpy(model->dimensao, STR_UNDEF);
    strcpy(model->area, STR_UNDEF);
    strcpy(model->construcao, STR_UNDEF);
    strcpy(model->endereco, STR_UNDEF);
    strcpy(model->contato, STR_UNDEF);
    strcpy(model->descricao, STR_UNDEF);
    strcpy(model->quartos, STR_UNDEF);
    strcpy(model->banheiros, STR_UNDEF);
    strcpy(model->garagens, STR_UNDEF);
    strcpy(model->suites, STR_UNDEF);
    strcpy(model->salas, STR_UNDEF);
    strcpy(model->escritorio, STR_NAO);
    strcpy(model->piscina, STR_NAO);
    strcpy(model->banheira, STR_NAO);
    strcpy(model->armario, STR_NAO);
    strcpy(model->churrasqueira, STR_NAO);
    strcpy(model->quintal, STR_NAO);
    strcpy(model->valor, STR_UNDEF);
    strcpy(model->financiamento, STR_NAO);
    strcpy(model->iptu, STR_UNDEF);
    strcpy(model->condominio, STR_UNDEF);
}

/* Model set value */
void model_set(Model *model, char *key, char *value) {
    if(strequals(key, KEY_CODIGO)) {
        strcpy(model->codigo, value);

    } else if(strequals(key, KEY_FINALIDADE)) {
        strcpy(model->finalidade, value);

    } else if(strequals(key, KEY_TIPO)) {
        strcpy(model->tipo, value);

    } else if(strequals(key, KEY_DIMENSAO)) {
        strcpy(model->dimensao, value);

    } else if(strequals(key, KEY_AREA)) {
        strcpy(model->area, value);

    } else if(strequals(key, KEY_CONSTRUCAO)) {
        strcpy(model->construcao, value);

    } else if(strequals(key, KEY_ENDERECO)) {
        strcpy(model->endereco, value);

    } else if(strequals(key, KEY_CONTATO)) {
        strcpy(model->contato, value);

    } else if(strequals(key, KEY_DESCRICAO)) {
        strcpy(model->descricao, value);

    } else if(strequals(key, KEY_QUARTOS)) {
        strcpy(model->quartos, value);

    } else if(strequals(key, KEY_BANHEIROS)) {
        strcpy(model->banheiros, value);

    } else if(strequals(key, KEY_GARAGENS)) {
        strcpy(model->garagens, value);

    } else if(strequals(key, KEY_SUITES)) {
        strcpy(model->suites, value);

    } else if(strequals(key, KEY_SALAS)) {
        strcpy(model->salas, value);

    } else if(strequals(key, KEY_ESCRITORIO)) {
        strcpy(model->escritorio, value);

    } else if(strequals(key, KEY_PISCINA)) {
        strcpy(model->piscina, value);

    } else if(strequals(key, KEY_BANHEIRA)) {
        strcpy(model->banheira, value);

    } else if(strequals(key, KEY_ARMARIO)) {
        strcpy(model->armario, value);

    } else if(strequals(key, KEY_CHURRASQUEIRA)) {
        strcpy(model->churrasqueira, value);

    } else if(strequals(key, KEY_QUINTAL)) {
        strcpy(model->quintal, value);

    } else if(strequals(key, KEY_VALOR)) {
        strcpy(model->valor, value);

    } else if(strequals(key, KEY_FINANCIAMENTO)) {
        strcpy(model->financiamento, value);

    } else if(strequals(key, KEY_IPTU)) {
        strcpy(model->iptu, value);

    } else if(strequals(key, KEY_CONDOMINIO)) {
        strcpy(model->condominio, value);

    } else {
        printf("ERRO: O XML gerado não corresponde ao modelo especificado; '%s'.\n", key);
    }
}

/* Utility: print tag to file */
void tag_print(FILE *output, unsigned tabs, char *tag, char *value) {
    /* Tabs */
    unsigned i=0;
    for(i=0; i<tabs; i++)
        fprintf(output, "\t");

    /* Tag + value */
    fprintf(output, "<an:%s>%s</an:%s>\n", tag, value, tag);
}

/* Utility: format 'Sim' and 'Não' */
char* strformatYN(char *str) {
    if(strequals(str, STR_NAO))
        strcpy(str, STR_NAO);
    if(strequals(str, STR_SIM))
        strcpy(str, STR_SIM);
    return str;
}

/* Model: write to XML */
void model_write(FILE *output, Model *model) {
    char buffer[128];

    fprintf(output, "\t<an:Imovel %s=\"%s\">\n", KEY_CODIGO, model->codigo);

    /* Model type tag start */
    if(strequals(model->tipo, "terreno"))
        fprintf(output, "\t\t<an:%s>\n", KEY_TERRENO);
    else
        fprintf(output, "\t\t<an:%s>\n", KEY_CONSTRUCAO);

    /* Common properties */
    tag_print(output, 3, KEY_TIPO, model->tipo);

    char street[32], number[32], block[32], city[32];
    strsplit2(model->endereco, ',', street, buffer);
    strsplit2(buffer, ',', number, buffer);
    strsplit2(buffer, ',', block, city);
    strtrim(street);
    strtrim(number);
    strtrim(block);
    strtrim(city);
    fprintf(output, "\t\t\t<an:%s>\n", KEY_ENDERECO);
    tag_print(output, 4, KEY_RUA, street);
    tag_print(output, 4, KEY_NUMERO, number);
    tag_print(output, 4, KEY_BAIRRO, block);
    tag_print(output, 4, KEY_CIDADE, city);
    fprintf(output, "\t\t\t</an:%s>\n", KEY_ENDERECO);

    char name[32], tel[32];
    strsplit2(model->contato, ',', name, buffer);
    strtrim(name);
    strtrim(buffer);
    strsplit2(buffer, ' ', buffer, tel);
    fprintf(output, "\t\t\t<an:%s>\n", KEY_CONTATO);
    tag_print(output, 4, KEY_NOME, name);
    tag_print(output, 4, KEY_TELEFONE, tel);
    fprintf(output, "\t\t\t</an:%s>\n", KEY_CONTATO);

    char width[8], length[8];
    strsplit2(model->dimensao, 'x', width, length);
    fprintf(output, "\t\t\t<an:%s>\n", KEY_DIMENSAO);
    tag_print(output, 4, KEY_LARGURA, width);
    tag_print(output, 4, KEY_COMPRIMENTO, length);
    fprintf(output, "\t\t\t</an:%s>\n", KEY_DIMENSAO);

    tag_print(output, 3, KEY_AREA, model->area);
    tag_print(output, 3, KEY_DESCRICAO, model->descricao);
    tag_print(output, 3, KEY_VALOR, model->valor);
    tag_print(output, 3, KEY_FINANCIAMENTO, strformatYN(model->financiamento));
    if(!strequals(model->iptu, STR_UNDEF))
        tag_print(output, 3, KEY_IPTU, model->iptu);
    if(!strequals(model->condominio, STR_UNDEF))
        tag_print(output, 3, KEY_CONDOMINIO, model->condominio);
    tag_print(output, 3, KEY_FINALIDADE, model->finalidade);

    /* Not terrain */
    if(!strequals(model->tipo, "terreno")) {
        tag_print(output, 3, KEY_CONSTRUCAO, model->construcao);
        if(atoi(model->quartos) > 0)
            tag_print(output, 3, KEY_QUARTOS, model->quartos);
        if(atoi(model->banheiros) > 0)
            tag_print(output, 3, KEY_BANHEIROS, model->banheiros);
        if(atoi(model->garagens) > 0)
            tag_print(output, 3, KEY_GARAGENS, model->garagens);
        if(atoi(model->suites) > 0)
            tag_print(output, 3, KEY_SUITES, model->suites);
        if(atoi(model->salas) > 0)
            tag_print(output, 3, KEY_SALAS, model->salas);

        if(!strequals(model->piscina, STR_NAO))
            tag_print(output, 3, KEY_PISCINA, strformatYN(model->piscina));
        if(!strequals(model->banheira, STR_NAO))
            tag_print(output, 3, KEY_BANHEIRA, strformatYN(model->banheira));
        if(!strequals(model->armario, STR_NAO))
            tag_print(output, 3, KEY_ARMARIO, strformatYN(model->armario));
        if(!strequals(model->churrasqueira, STR_NAO))
            tag_print(output, 3, KEY_CHURRASQUEIRA, strformatYN(model->churrasqueira));
        if(!strequals(model->quintal, STR_NAO))
            tag_print(output, 3, KEY_QUINTAL, strformatYN(model->quintal));
    }

    /* Model type tag end */
    if(strequals(model->tipo, "terreno"))
        fprintf(output, "\t\t</an:%s>\n", KEY_TERRENO);
    else
        fprintf(output, "\t\t</an:%s>\n", KEY_CONSTRUCAO);

    /* Model tag end */
    fprintf(output, "\t</an:Imovel>\n\n");
}

/* Model: debug print */
void model_print(Model *model) {
    printf("Codigo: %s\n", model->codigo);
    printf("Finalidade: %s\n", model->finalidade);
    printf("Tipo: %s\n", model->tipo);
    printf("Dimensao: %s\n", model->dimensao);
    printf("Area: %s\n", model->area);
    printf("Construção: %s\n", model->construcao);
    printf("Endereco: %s\n", model->endereco);
    printf("Contato: %s\n", model->contato);
    printf("Descricao: %s\n", model->descricao);
    printf("Quartos: %s\n", model->quartos);
    printf("Banheiros: %s\n", model->banheiros);
    printf("Garagens: %s\n", model->garagens);
    printf("Suites: %s\n", model->suites);
    printf("Salas: %s\n", model->salas);
    printf("Escritorio: %s\n", model->escritorio);
    printf("Piscina: %s\n", model->piscina);
    printf("Banheira: %s\n", model->banheira);
    printf("Armario: %s\n", model->armario);
    printf("Churrasqueira: %s\n", model->churrasqueira);
    printf("Quintal: %s\n", model->quintal);
    printf("Valor: %s\n", model->valor);
    printf("Financiamento: %s\n", model->financiamento);
    printf("IPTU: %s\n", model->iptu);
    printf("Condominio: %s\n", model->condominio);
}

/* Main function */
int main(int argc, char *argv[]) {

    /* Check arguments */
    if(argc < 3) {
        printf("Usage: ./XMLParser <input bd file> <output xml file>\n\n");
        return EXIT_FAILURE;
    }

    /* Parse arguments */
    char *inputFile = argv[1];
    char *outputFile = argv[2];

    /* Debug */
    printf("Input BD file: %s\n", inputFile);
    printf("Output XML file: %s\n\n", outputFile);

    /* Open files */
    FILE *input = fopen(inputFile, "r");
    if(input==NULL) {
        printf("Failed to open BD input file '%s'.\n", inputFile);
        return EXIT_FAILURE;
    }

    FILE *output = fopen(outputFile, "w");
    if(output==NULL) {
        printf("Failed to open XML output file '%s'.\n", inputFile);
        return EXIT_FAILURE;
    }

    /* Write XML start flag */
    fprintf(output, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    fprintf(output, "<an:Anuncios xmlns:an=\"http://www.example.com/Imoveis\"\n\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n\txsi:schemaLocation=\"http://www.example.com/Imoveis imoveis.xsd\">\n\n");

    /* Create model */
    Model model;
    model_initialize(&model);
    int firstModel = 1;

    /* Read line by line until EOF */
    char buffer[128];
    char key[32];
    char value[96];
    while(fgets(buffer, 128, input) != NULL) {

        /* Check empty line */
        if(strlen(buffer)<=1)
            continue;

        /* Split string */
        int split = strsplit2(buffer, STR_DELIMITER, key, value);
        if(split==EXIT_FAILURE) {
            printf("Error while parsing input BD file, read: '%s'.\n", buffer);
            return EXIT_FAILURE;

        } else {
            /* Remove \n and trim strings */
            strtrim(key);
            strtrim(value);
            if(value[strlen(value)-1]=='\n')
                value[strlen(value)-1] = '\0';

            /** DEBUG **/
            /*printf("Key: '%s', value: '%s'\n", key, value);*/

            /* Check if it's a new model definition */
            if(strequals(key, KEY_CODIGO)) {

                /* Check if has to save the current model */
                if(firstModel==0)
                    model_write(output, &model);

                /* Initialize and set code */
                model_initialize(&model);
            }

            /* Save info */
            model_set(&model, key, value);
            firstModel = 0;
        }

    }

    /* Write last model */
    model_write(output, &model);

    /* Write XML end flags */
    fprintf(output, "</an:Anuncios>\n");

    /* Close files */
    fclose(input);
    fclose(output);

    /* End */
    printf("XML file generated!\n");

    return EXIT_SUCCESS;
}
