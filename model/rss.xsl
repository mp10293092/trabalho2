<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:an="http://www.example.com/Imoveis">
<xsl:output method="xml"/>
    <xsl:template match="/">
    <rss version="2.0"/>
    <channel>
        <title>Sistema de Imobiliária</title>
        <link>http://www.example.com/imobiliaria</link>
        <description>Trabalho de Hipermídia</description>
        <xsl:for-each select="an:Anuncios/an:Imovel/*">
        <item>
            <title><xsl:value-of select="an:Finalidade"/>: <xsl:value-of select="an:Tipo"/></title>
            <link/>
            <description>Bairro: <xsl:value-of select="an:Endereco/an:Bairro"/>, Valor: <xsl:value-of select="an:Valor"/>, Contato: <xsl:value-of select="an:Contato/an:Telefone"/></description>
        </item>
        </xsl:for-each>
    </channel>
    </xsl:template>
</xsl:stylesheet>
