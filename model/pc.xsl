<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:an="http://www.example.com/Imoveis" exclude-result-prefixes="an">
<xsl:output method="html"/>
    <xsl:template match="/">
    <html>
        <head>
            <link rel="stylesheet" type="text/css" href="pc.css"/>
        </head>
        <body>
        <table>
        <tr>
            <th>Código</th>
            <th>Finalidade</th>
            <th>Tipo</th>
            <th>Endereço</th>
            <th>Contato</th>
            <th>Dimensão</th>
            <th>Área (m<sup>2</sup>)</th>
            <th>Valor</th>
            <th>Financiamento</th>
            <th>Quartos</th>
            <th>Banheiros</th>
            <th>Garagens</th>
            <th>Suites</th>
            <th>Salas</th>
            <th>Adicionais</th>
        </tr>
        <xsl:for-each select="an:Anuncios/an:Imovel/*">
        <tr>
            <td><xsl:value-of select="../@Codigo"/></td>
            <td><xsl:value-of select="an:Finalidade"/></td>
            <td><xsl:value-of select="an:Tipo"/></td>
            <td><xsl:value-of select="an:Endereco"/></td>
            <td><xsl:value-of select="an:Contato"/></td>
            <td><xsl:value-of select="an:Dimensao/an:Largura"/>m x <xsl:value-of select="an:Dimensao/an:Comprimento"/>m</td>
            <td><xsl:value-of select="an:Area"/><xsl:if test="an:Tipo != 'terreno'">, <xsl:value-of select="an:Construcao"/> construídos</xsl:if></td>
            <td>R$<xsl:value-of select="an:Valor"/><xsl:if test="an:IPTU"> + R$<xsl:value-of select="an:IPTU"/> IPTU</xsl:if><xsl:if test="an:Condominio"> + R$<xsl:value-of select="an:Condominio"/> condomínio</xsl:if></td>
            <td>
            <xsl:choose>
                <xsl:when test="an:Financiamento = 'Sim'"> com </xsl:when>
                <xsl:otherwise> sem </xsl:otherwise>
            </xsl:choose>
            </td>
            <td>
            <xsl:choose>
                <xsl:when test="an:Quartos"><xsl:value-of select="an:Quartos"/></xsl:when>
                <xsl:otherwise><xsl:if test="Tipo != terreno">Não especificado</xsl:if></xsl:otherwise>
            </xsl:choose>
            </td>
            <td>
            <xsl:choose>
                <xsl:when test="an:Banheiros"><xsl:value-of select="an:Banheiros"/></xsl:when>
                <xsl:otherwise><xsl:if test="Tipo != terreno">Não especificado</xsl:if></xsl:otherwise>
            </xsl:choose>
            </td>
            <td>
            <xsl:choose>
                <xsl:when test="an:Garagens"><xsl:value-of select="an:Garagens"/></xsl:when>
                <xsl:otherwise><xsl:if test="Tipo != terreno">Não especificado</xsl:if></xsl:otherwise>
            </xsl:choose>
            </td>
            <td>
            <xsl:choose>
                <xsl:when test="an:Suites"><xsl:value-of select="an:Suites"/></xsl:when>
                <xsl:otherwise><xsl:if test="Tipo != terreno">Não especificado</xsl:if></xsl:otherwise>
            </xsl:choose>
            </td>
            <td>
            <xsl:choose>
                <xsl:when test="an:Salas"><xsl:value-of select="an:Salas"/></xsl:when>
                <xsl:otherwise><xsl:if test="Tipo != terreno">Não especificado</xsl:if></xsl:otherwise>
            </xsl:choose>
            </td>
            <td>
            <xsl:if test="an:Piscina or an:Banheira or an:Armario or an:Churrasqueira or an:Quintal">
                <xsl:if test="an:Escritorio"> escritorio</xsl:if>
                <xsl:if test="an:Piscina"><xsl:if test="an:Escritorio">,</xsl:if> piscina</xsl:if>
                <xsl:if test="an:Banheira"><xsl:if test="an:Piscina or an:Escritorio">,</xsl:if> banheira</xsl:if>
                <xsl:if test="an:Armario"><xsl:if test="an:Banheira or an:Piscina or an:Escritorio">,</xsl:if> armario</xsl:if>
                <xsl:if test="an:Churrasqueira"><xsl:if test="an:Armario or an:Banheira or an:Piscina or an:Escritorio">,</xsl:if> churrasqueira</xsl:if>
                <xsl:if test="an:Quintal"><xsl:if test="an:Churrasqueira or an:Armario or an:Banheira or an:Piscina or an:Escritorio">,</xsl:if> quintal</xsl:if>
            </xsl:if>
            </td>
        </tr>
        </xsl:for-each>
        </table>
        </body>
    </html>
    </xsl:template>
</xsl:stylesheet>
