<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:an="http://www.example.com/Imoveis" exclude-result-prefixes="an">
<xsl:output method="html"/>
    <xsl:template match="/">
    <html><body>
        <xsl:for-each select="an:Anuncios/an:Imovel/*">
            <h4><xsl:value-of select="an:Finalidade"/>: <xsl:value-of select="an:Tipo"/> - Código <xsl:value-of select="../@Codigo"/></h4><p>
            Endereço: <xsl:value-of select="an:Endereco"/><br/>
            Contato: <xsl:value-of select="an:Contato"/><br/>
            Dimensões do terreno: <xsl:value-of select="an:Dimensao/an:Largura"/>m x <xsl:value-of select="an:Dimensao/an:Comprimento"/>m, <xsl:value-of select="an:Area"/>m<sup>2</sup>
            <xsl:if test="an:Tipo != 'terreno'">, <xsl:value-of select="an:Construcao"/>m<sup>2</sup> construídos</xsl:if>
            <br/>
            Valor: R$<xsl:value-of select="an:Valor"/>
            <xsl:choose>
                <xsl:when test="an:Financiamento = 'Sim'"> com </xsl:when>
                <xsl:otherwise> sem </xsl:otherwise>
            </xsl:choose>
            financiamento
            <xsl:if test="an:IPTU">, R$<xsl:value-of select="an:IPTU"/> de IPTU</xsl:if>
            <xsl:if test="an:Condominio"> + R$<xsl:value-of select="an:Condominio"/> de condomínio</xsl:if>
            <br/>
            <xsl:if test="an:Quartos"><xsl:value-of select="an:Quartos"/> quarto(s)</xsl:if>
            <xsl:if test="an:Banheiros"><xsl:if test="an:Quartos">, </xsl:if><xsl:value-of select="an:Banheiros"/> banheiro(s)</xsl:if>
            <xsl:if test="an:Garagens"><xsl:if test="an:Quartos or an:Banheiros">, </xsl:if><xsl:value-of select="an:Garagens"/> garagem(ns)</xsl:if>
            <xsl:if test="an:Suites"><xsl:if test="an:Quartos or an:Banheiros or an:Garagens">, </xsl:if><xsl:value-of select="an:Suites"/> suíte(s)</xsl:if>
            <xsl:if test="an:Salas"><xsl:if test="an:Quartos or an:Banheiros or an:Garagens or an:Suites">, </xsl:if><xsl:value-of select="an:Salas"/> sala(s)</xsl:if>
            <br/>
            <xsl:if test="an:Piscina or an:Banheira or an:Armario or an:Churrasqueira or an:Quintal">
                Com
                <xsl:if test="an:Escritorio"> escritorio</xsl:if>
                <xsl:if test="an:Piscina"><xsl:if test="an:Escritorio">,</xsl:if> piscina</xsl:if>
                <xsl:if test="an:Banheira"><xsl:if test="an:Piscina or an:Escritorio">,</xsl:if> banheira</xsl:if>
                <xsl:if test="an:Armario"><xsl:if test="an:Banheira or an:Piscina or an:Escritorio">,</xsl:if> armario</xsl:if>
                <xsl:if test="an:Churrasqueira"><xsl:if test="an:Armario or an:Banheira or an:Piscina or an:Escritorio">,</xsl:if> churrasqueira</xsl:if>
                <xsl:if test="an:Quintal"><xsl:if test="an:Churrasqueira or an:Armario or an:Banheira or an:Piscina or an:Escritorio">,</xsl:if> quintal</xsl:if>
            </xsl:if>
            </p>
        </xsl:for-each>
    </body></html>
    </xsl:template>
</xsl:stylesheet>
