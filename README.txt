SCC0661 - Multimídia e Hipermídia
- Trabalho 2 -
 
Guilherme Caixeta de Oliveira - nUSP: 8504368
Henrique Cintra Miranda de Souza Aranha - nUSP: 8551434

ORGANIZAÇÃO DE PASTAS:
- model: schema e transformações;
- inputs: inputs de teste;
- XMLParser: código do transformador de entrada de texto em XML.

COMPILAÇÃO:
Dentro da pasta do XMLParser, utilize o Makefile (make).
Será gerado o arquivo binário com nome "XMLParser" na pasta bin/.

EXECUÇÃO:
$ ./XMLParser input.txt output.xml

VALIDAÇÃO SCHEMA:
$ xmllint --noout -schema model/imoveis.xsd output.xml

TRANSFORMAÇÃO PC:
$ xsltproc -o pc.html model/pc.xsl output.xml

TRANSFORMAÇÃO MOBILE:
$ xsltproc -o mobile.html model/mobile.xsl output.xml

TRANSFORMAÇÃO RSS:
$ xsltproc -o feed.rss model/rss.xsl output.xml

